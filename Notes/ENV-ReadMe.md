### Sed - Stream Editor
for filtering and transforming text

```
sed -i 's/word1/word2/g' inputfile
```

- `-i` for edit in place (edit the same file, don't create a new one)
- `s` for subtitute
- `g` for global replacement

### Predefined ENV Documents

```
https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
```

Sample How to use ENV in updating the version of the app

1. Add an html file in the page `<div>Version: %%VERSION%%</div>`
2. Then go to the ci and add these script command to the job
```
- echo $CI_COMMIT_SHORT_SHA
- sed -i "s/%%VERSION%%/$CI_COMMIT_SHORT_SHA/" ./public/index.html
```

### Environments in Gitlab
- Enviroments allow you to control the continous delivery/deployment process
- Easily track deployment
- You will know exactly what was deployed and on which enviroment
- You will have a full history of your deployments


