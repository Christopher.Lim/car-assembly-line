### What is CI/CD?
Continuos Integration (CI)
- Practice of continuosly integrating code changes
- Ensures that the project can still be built/compiled
- Ensures that any changes pass all tests, guidelines, and code compliance standards

Continuous Delivery (CD)
- Ensures that the software can be deployed anytime to production
- Commonly, the latest version is deployed to a testing or staging system

```
CI/CD Pipeline
Developer --<CODE/>--(CI PIPELINE)<BUILD/>--<CODE QUALITY/>--<TESTS/>--<PACKAGE/>(CI PIPELINE/)--(CD PIPELINE)<REVIEW TEST/>--<STAGING/>--<PRODUCT MANAGER/>--<PRODUCTION/>(CD PIPELINE/)
```

Advantages of Continuous Integration (CI)
- Errors are detected early in the development process
- Reduces integration problems
- Allows developers to work faster

Advantages of Continuous Delivery and Continuos Deployment (CD)
- Ensures that every change is releasable by testing that it can be deployed
- Reduced risk of a new Deployment
- Delivers value much faster



























