### Building the project locally

The Build Step
- Most project have a production build step
- Developers write code in programming langauges like Java, Javascript
- The source code needs to be processed further so it can be deployed

The Build Step in Gatsby
- Produces static HTML and JavaScript files
- CSS/JavaScript files are merged and compressed in otder to reduce the website download size
- Command `gatsby build`

