### Git Commands

To clone a repo
```
git clone ssh:key
```

To check the status
```
git status
```

To create a new branch
```
git checkout -b <branch name>
```

To checkout from a branch
```
git checkout <branch name>
```

To pull latest changes
```
git pull origin <branch name>
```

To do a rebase
```
git pull rebase
```

To point exisiting repo at a new origin
```
git remote rename origin old-origin
git remote add origin <git url here>
git branch --set-upstream-to=origin/<branch name> <branch name>
```

To create a pull request
```
git add .
git commit -m 'message here'
git push origin <branch name>
```
