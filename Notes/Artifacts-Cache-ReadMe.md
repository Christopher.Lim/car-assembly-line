### Artifacts vs Cache

Artifacts
- An artificat is usually the output of a build tool
- In Gitlab CI, artifacts are designed to save some compiled/generated part of the build
- Artifacts can be used to pass data between stages/jobs

Caches
- Caches are not to be used to store build results
- Cache should only be used as a temporary storage for project dependencies

