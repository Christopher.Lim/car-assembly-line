### surge.sh
What is surge.sh
- Cloud platform for serverless deployments
- Easy to use and configure
- Simple deployment process
- Ideal for static websites

Surge Documents
```
https://surge.sh/help/getting-started-with-surge
```

How to install surge
```
npm install --gloabl surge
```

How to use surge
1. Change directory to the folder that has your html/css that was build in
2. In your terminal type `surge`
3. Enter your email and password.
4. It will then generate a random surge domain for you that is available for couple of days.

How to generate surge token
```
surge token
```
