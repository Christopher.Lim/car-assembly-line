### Node.js and NPM

Node.js
- Node.js is a JavaScript runtime environment
- It can be executed without using a browser

Node Package Manager (npm)
- It is a repository of projects and has knowledge of what requirements each projects has
- Usually each project/tool has other dependencies that need to be installed

### How to install Node.js

#### MAC OS

```
brew install Node
```

#### Windows

Chocolately
```
cinst nodejs
# or for full install with npm
cinst nodejs.install
```

Scoop
```
scoop install nodejs
```

Example of Node.js Application
```
const http = require('http');

const hostname = '127.0.0.1'
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World!\n')
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:{$port
});
```

How to run Node.js scripts from the command line
```
node app.js
```

To install a package
```
npm install <package>
```

To install a package as dev dependencies
```
npm install <package> --save-dev
```

To uninstall a package
```
npm uninstall <package>
```

Semantic Versioning
All node package versions have 3 digits lets call them `x.y.z`
- the first or x is the major versions
- the second or y is the minor versions
- the third or z is the patch versions

Example of node package versions
```
node 12.3.1
```

The rules use of symbols that found in `package.json`
- `^`:  It will only update either the minor or the patch (^0.3.1 into ^0.3.2 or ^2.3.1 to ^2.4.0)
- `~`: It will only update the patch
- `>`: It will only update higher version
- `>=`: It will update higher or equal version
- `<=`: It will accept lower or equal version
- `<`: It will accept lower version
- `=`: It will only accept exact version
- `-`: It will accept range of versions (2.1.0 - 2.4.0)
- `||`: It will combine sets of versions (< 2.1 || > 2.6)
- `no symbol`: It will accept only the version you specify
- `latest`: It will use the latest version available
