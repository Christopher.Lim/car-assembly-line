### What is Docker?
- Technology based on containers which allows virtualization

Docker images
- An image is a file with a set instructions on how to package up code or utilities and all dependencies
- When the Docker executes an image, it becomes a container that is similar to a virtual machine

Docker for deploying applications
- Docker allows us to package applications and all necessary dependencies and to easily deploy them

Docker for Continous Integration Servers
- Can use latest version of Node.js

Docker Documentation link
```
https://docs.docker.com/docker-for-mac/install/
```

