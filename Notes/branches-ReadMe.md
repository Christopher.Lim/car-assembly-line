### Using branches
- Avoids breaking the master
- Breaking the master branch is costly and should be avoided
- Ensures that CD is always possible

### Using branches
- Each feature/task/bugfix could be done on a separate branch
- Once the work is done, tested and reviewed, it can be merged back to master

### Branching models
- The most known strategy is using GitFlow
- You are free to use with model suits you best
- Just avoid using only one branch

