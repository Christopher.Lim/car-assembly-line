### Dynamic environments
- Staging and Production are for the master branch
- To deploy a branch to an environment, we also need another environment (let's call this environment review)
- As we actually work with multiple branches at a time, we need to dynamically create enviroments for each branch
- As each Merge Request/branch is deployed to an environment, we can easily review the changes made
- Sometimes it makes sense to run additional tests on a system that was deployed
- Changes can be reviewed by non-developers as well (Testers, PO/PM, Domain Expers and so on)
