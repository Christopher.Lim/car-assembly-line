### .gitlab-ci.yml notes

```
stages: // stages tells gitlab what order the job should run first
    - build
    - test


build the car: // Jobs name
    stage: build //sets the job in the build group
    script: // commands will be used to run the job
        - mkdir build // mkdir stands for making a directory or a folder
        - cd build  // cd meands change directory
        - touch car.txt // touch stands for creating a file
        - echo "chassis" >> car.txt //echo is used to display a lien of text
        - echo "engine" >> car.txt // single > overwrites the whole file and double >> appends the content of the file
        - echo "wheels" >> car.txt
    artifacts: //artifacts tells the job to save file creation of car.txt
        paths: // path where the artifacts will be added
            - build/ //folder name

test the car: // Job number 2
    stage: test
    script:
        - ls // ls means list of file and folders
        - test -f build/car.txt / test command is used to verify that car.txt was created and the -f flag is used to check if the file exists
        - cd build
        - cat car.txt //cat is used to display or show the entire content
        - grep "chassis" car.txt // grep is used to search lines that match a regular expression
        - grep "engine" car.txt
        - grep "wheels" car.txt
```
#### notes
GitLab Runner is a tool used to run your jobs in GitLab.

### Bash command commonly used
1. ls - display the list of files and directory
2. cd - change directory is used to navigate through files
3. mkdir - make directories command is used to make a folder
4. touch - create a file
5. cat - concatenate is used to print files in stdout
6. mv - moves  is used to move files
7. cp - copies is used to copies files and folder
8. rm  - removes file and folders
9. chmod - change mode, changes set permissions for read, write and execute. 
    - 777 - anyone can read, write, execute
    - 755 - should only read and execute
    - 700 - only admin can do something to the file
10. man - manuals display the manuals
11. curl - downloads the file

### Generating an ssh-key for gitlab
To generate a ssh keygen
```
ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
```

To copy your ssh

macOS:
```
pbcopy < ~/.ssh/id_ed25519.pub
```

Git Bash on Windows:
```
cat ~/.ssh/id_ed25519.pub | clip
```
